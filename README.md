## Assymetric proxy

This proxy script allows modifying both header and bodies of HTTP requests and responses by directly modifying the respective variables, for example, encrypting/decrypting data or hiding the user identity under the header "User-Agent".

This implementation assumes that Rafal receives and send encrypted data and therefore, the main purpose of the proxy server is to encrypt the client's message to Rafal on send and decrypt the message on receival from Rafal before sending the clear message to the user.

To make the test:

1. Launch the script: `python3 proxy.py [logFolder] [delete]`
2. Use any Rafal GET/POST request (the test was made on POST /login request) and specify the credentials (localhost and port 8001).
 
--- 


