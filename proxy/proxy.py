import os
import sys
import shutil
import ast
import base64
import json
import tornado.ioloop
import tornado.web
from tornado.httpclient import AsyncHTTPClient


forward = "http://51.15.220.51:8080"

# b64str = base64.encodestring('%s:%s' % (username, password)).replace('\n', '')

path = "request_log/" + sys.argv[1]
if len(sys.argv) > 2:
    assert sys.argv[2] == "delete"
    try:
        shutil.rmtree(path)
    except OSError:
        pass
os.mkdir(path)
nth_request = 0


async def get_responses(self):
    # Modifying headers as dict
    headers = dict(self.request.headers.items())

    # Modifying the request as dict
    body = ast.literal_eval(self.request.body.decode()) # turn into dict
    body = json.dumps(body, indent=2).encode('utf-8')   # turn into bytes

    print("_________________________________________________________") # Debugging separator for output

    r = await AsyncHTTPClient().fetch(
        forward + self.request.uri,
        method=self.request.method,
        body=body,
        headers=headers,
        allow_nonstandard_methods=True,
        raise_error=False,
    )
    global nth_request
    with open(path + "/" + str(nth_request) + ".q.xml", "w") as f:
        f.write(self.request.body.decode())

    with open(path + "/" + str(nth_request) + ".r.xml", "w") as f:
        f.write(r.body.decode())
    nth_request += 1

    print(type(r.body))


    return r


def set_headers(self, response):
    for h, hv in response.headers.items():
        if h not in (
            "Content-Length",
            "Transfer-Encoding",
            "Content-Encoding",
            "Connection",
        ):
            self.set_header(h, hv)


class ProxyHandler(tornado.web.RequestHandler):
    async def get(self):
        response = await get_responses(self)
        if response.code != 200:
            set_headers(self, response)
            self.write(response.body)
            return

        self.set_status(response.code)

        # Modifying header and response
        set_headers(self, response)
        self.write("String".encode())

    async def post(self):
        return await self.get()


tornado.web.Application([(r"/.*", ProxyHandler)], debug=True).listen(8001)
tornado.ioloop.IOLoop.current().start()
